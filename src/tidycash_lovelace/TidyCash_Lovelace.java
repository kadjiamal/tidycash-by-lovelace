package tidycash_lovelace;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *  Tugas Besar RPL-FPA
 *  Tidy Cash
 * 
 *  20523224 Muhammad Ichlasul Amal Yulianto
 *  20523133 Nadia Rahmi Nur Anggraini
 *  20523154 Ilham Arfianto
 *  20523225 Dhimassultan Mahamrta Dutasmara
 *  20523233 Lalam Fathonah Fadhillah
 */
public class TidyCash_Lovelace extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.getIcons().add(new Image("/images/logo.png"));
        stage.setTitle("TidyCash");
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
